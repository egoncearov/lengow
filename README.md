### Running development server

Use command `docker-compose up --build` to bring the dev server up. It will be available on port 8000.
The following endpoint were implemented:
- http://127.0.0.1:8000/orders
- http://127.0.0.1:8000/orders/111-2222222-3333333 (use the value of `order_id` field here)

### Importing orders

Use command `docker-compose run -it api python manage.py import_orders`

### Running unit tests

Use command `docker-compose run -it api pytest`