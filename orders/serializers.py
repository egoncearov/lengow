from rest_framework.serializers import ModelSerializer

from orders import models


class OrderSerializer(ModelSerializer):
    class Meta:
        model = models.Order
        fields = [
            "order_id",
            "mrid",
            "refid",
            "id_flux",
            "marketplace",
            "status_marketplace",
            "status_lengow",
            "purchase_datetime",
        ]
