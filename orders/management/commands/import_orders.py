import datetime
import xml.etree.ElementTree as ET
from typing import Any, Optional

import requests
from django.core.management.base import BaseCommand, CommandError
from django.db import IntegrityError

from orders.models import Order


class Command(BaseCommand):
    help = "Import orders into database"

    base_url = "http://test.lengow.io/orders-test.xml"

    def handle(self, *args: Any, **options: Any) -> None:
        r = requests.get(self.base_url)
        try:
            r.raise_for_status()
        except requests.HTTPError as e:
            raise CommandError(e)
        self._parse_orders(r.text)

    def _parse_orders(self, orders_xml: str) -> None:
        root = ET.fromstring(orders_xml)
        for order_element in root.findall("./orders/order"):
            self._create_order_from_xml(order_element)

    def _create_order_from_xml(
        self, order_element: ET.Element
    ) -> Optional[Order]:
        order_data = {}
        fields_mapping = {
            "marketplace": "marketplace",
            "idFlux": "id_flux",
            "order_id": "order_id",
            "order_mrid": "mrid",
            "order_refid": "refid",
            "order_purchase_date": "purchase_date",
            "order_purchase_heure": "purchase_time",
        }
        for child in order_element:
            if child.tag in fields_mapping:
                order_data[fields_mapping[child.tag]] = child.text or ""
            elif child.tag == "order_status":
                for status_child in child:
                    if status_child.tag == "marketplace":
                        order_data["status_marketplace"] = (
                            status_child.text or ""
                        )
                    elif status_child.tag == "lengow":
                        order_data["status_lengow"] = status_child.text or ""
        if order_data.get("purchase_date") and order_data.get("purchase_time"):
            order_data["purchase_datetime"] = datetime.datetime.strptime(
                f"{order_data['purchase_date']} {order_data['purchase_time']}",
                "%Y-%m-%d %H:%M:%S",
            ).replace(tzinfo=datetime.timezone.utc)
        order_data.pop("purchase_date", None)
        order_data.pop("purchase_time", None)
        order_id = order_data.pop("order_id")
        try:
            order, _ = Order.objects.update_or_create(
                order_id=order_id, defaults=order_data
            )
        except (IntegrityError, ValueError) as e:
            self.stderr.write(f"Failed to process order: {str(e)}")
            return
        self.stdout.write(f"Successfully imported order {order}")
        return order
