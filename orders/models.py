from django.db import models


class Order(models.Model):
    marketplace = models.CharField(max_length=32)
    id_flux = models.IntegerField()
    status_marketplace = models.CharField(max_length=32, blank=True)
    status_lengow = models.CharField(max_length=32, blank=True)
    order_id = models.CharField(primary_key=True, max_length=32)
    mrid = models.CharField(unique=True, max_length=32)
    refid = models.CharField(unique=True, max_length=32)
    purchase_datetime = models.DateTimeField(null=True)

    def __repr__(self) -> str:
        return f"Order {self.order_id}"

    def __str__(self) -> str:
        return (
            f"Order {self.order_id} from {self.marketplace} "
            f"created on {self.purchase_datetime}"
        )
