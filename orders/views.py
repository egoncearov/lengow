from rest_framework.viewsets import ReadOnlyModelViewSet

from orders.models import Order
from orders.serializers import OrderSerializer


class OrderViewSet(ReadOnlyModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
