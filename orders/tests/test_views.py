import pytest
from django.shortcuts import reverse
from rest_framework.test import APIClient

from orders.tests.factories import OrderFactory


@pytest.fixture()
def api_client() -> APIClient:
    client = APIClient()
    yield client


@pytest.mark.django_db
def test_get_orders_list(api_client: APIClient) -> None:
    orders = OrderFactory.create_batch(size=3)
    response = api_client.get(reverse("order-list"))
    assert response.status_code == 200
    response_orders = response.data
    assert len(response_orders) == 3
    assert {order["order_id"] for order in response_orders} == {
        order.order_id for order in orders
    }


@pytest.mark.django_db
def test_get_existing_order(api_client: APIClient) -> None:
    order = OrderFactory.create()
    response = api_client.get(reverse("order-detail", args=(order.order_id,)))
    assert response.status_code == 200
    assert response.data["order_id"] == order.order_id


@pytest.mark.django_db
def test_get_non_existing_order(api_client: APIClient) -> None:
    response = api_client.get(reverse("order-detail", args=("not exists",)))
    assert response.status_code == 404
