import datetime

import factory
import factory.fuzzy


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "orders.Order"

    marketplace = "amazon"
    id_flux = factory.Sequence(lambda n: n)
    status_marketplace = "open"
    status_lengow = "new"
    order_id = factory.Sequence(lambda n: f"id-{n}")
    mrid = factory.Sequence(lambda n: f"mrid-{n}")
    refid = factory.Sequence(lambda n: f"refid-{n}")
    purchase_datetime = factory.fuzzy.FuzzyDateTime(
        start_dt=datetime.datetime.now(tz=datetime.timezone.utc)
    )
