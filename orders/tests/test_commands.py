import datetime
import pathlib
from unittest.mock import MagicMock, patch

import pytest
from django.core.management import call_command

from orders.management.commands.import_orders import Command
from orders.models import Order
from orders.tests.factories import OrderFactory


@pytest.fixture()
def orders_xml() -> str:
    with open(pathlib.Path(__file__).parent / "orders.xml") as f:
        yield f.read()


def test_import_orders_parse_is_called():
    with (
        patch(
            "orders.management.commands.import_orders.requests"
        ) as requests_mock,
        patch(
            "orders.management.commands.import_orders.Command._parse_orders"
        ) as parse_orders_mock,
    ):
        response_text = "42"
        response_mock = MagicMock()
        response_mock.text = response_text
        requests_mock.get.return_value = response_mock
        call_command("import_orders")
        parse_orders_mock.assert_called_once_with(response_text)


@pytest.mark.django_db
def test_parsing_orders_order_not_exists(orders_xml):
    command = Command()
    command._parse_orders(orders_xml)
    order = Order.objects.get(pk="111-2222222-3333333")
    assert order.marketplace == "amazon"
    assert order.mrid == "111-2222222-3333333"
    assert order.refid == "111-2222222-3333333"
    assert order.id_flux == 88827
    assert order.status_marketplace == "accept"
    assert order.status_lengow == "processing"
    assert order.purchase_datetime == datetime.datetime(
        2014, 10, 21, 14, 59, 51, tzinfo=datetime.timezone.utc
    )


@pytest.mark.django_db
def test_parsing_orders_order_already_exists(orders_xml):
    existing_order = OrderFactory.create(order_id="111-2222222-3333333")
    command = Command()
    command._parse_orders(orders_xml)
    assert Order.objects.count() == 1
    existing_order.refresh_from_db()
    assert existing_order.marketplace == "amazon"
    assert existing_order.mrid == "111-2222222-3333333"
    assert existing_order.refid == "111-2222222-3333333"
    assert existing_order.id_flux == 88827
    assert existing_order.status_marketplace == "accept"
    assert existing_order.status_lengow == "processing"
    assert existing_order.purchase_datetime == datetime.datetime(
        2014, 10, 21, 14, 59, 51, tzinfo=datetime.timezone.utc
    )


@pytest.mark.django_db
def test_parsing_orders_fails_constraint_violation(orders_xml):
    OrderFactory.create(order_id="does not matter", mrid="111-2222222-3333333")
    command = Command()
    command._parse_orders(orders_xml)
    assert Order.objects.count() == 1
    assert not Order.objects.filter(order_id="111-2222222-3333333").exists()
